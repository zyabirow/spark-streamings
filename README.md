## Initialize Azure infrastructure via terraform 

* Change directory to terraform directory
```bash
pushd ./terraform
```

* Slightly change to terraform/main.tf file in `terraform.backend` block:  
You will be asked to add these parameters anyway, so it looks for me to be best practice to add them in main.tf instead of prompting it in Shell.  
These parameters are:  
> name of the *resource group* for your azure account    

> *storage account name* - to store terraform backend  
 
* Initialize Terraform
```bash
terraform init
```

* Evaluate plan:
  ```bash
  terraform plan -out tfplan
  ```

* Apply plan:
  ```bash
  terraform apply "tfplan"
  ```

## Notebook anatomy and Results
* First part of databricks notebook consist of library importing and session configuration steps:  
![screenshots](screenshots/m13_1.png)   
![screenshots](screenshots/m13_2.png)   
* After source data directory is set we are able to read stream data from there:
  > Note: reading streamed data from parquet source requires schema description. It is possible to set it in different ways but i just took it from the source itself by reading it as a table.  
![screenshots](screenshots/m13_3.png) 
* To get data aggregation we need to run this code snippet:  
![screenshots](screenshots/m13_4.png)   

* it creates new columns with average, minimum and maximum temperature. It also calculates hotel count city by city, day by day:
![screenshots](screenshots/m13_5.png)  

* Here is the execution plan:
![screenshots](screenshots/m13_6.png)  


```
== Parsed Logical Plan ==
'Sort ['Hotel_count DESC NULLS LAST], true
+- Aggregate [city#200, wthr_date#207], [city#200, wthr_date#207 AS date#225, approx_count_distinct(id#203, 0.05, 0, 0) AS Hotel_count#345L, round(avg(avg_tmpr_c#198), 1) AS Average_temp#347, max(avg_tmpr_c#198) AS Max_temp#349, min(avg_tmpr_c#198) AS Min_temp#351]
   +- StreamingRelation DataSource(org.apache.spark.sql.SparkSession@2f6fabc3,delta,List(),None,List(),None,Map(path -> /tmp/delta/bronze/data),None), tahoe, [address#197, avg_tmpr_c#198, avg_tmpr_f#199, city#200, country#201, geoHash#202, id#203, latitude#204, longitude#205, name#206, wthr_date#207, year#208, month#209, day#210]

== Analyzed Logical Plan ==
city: string, date: string, Hotel_count: bigint, Average_temp: double, Max_temp: double, Min_temp: double
Sort [Hotel_count#345L DESC NULLS LAST], true
+- Aggregate [city#200, wthr_date#207], [city#200, wthr_date#207 AS date#225, approx_count_distinct(id#203, 0.05, 0, 0) AS Hotel_count#345L, round(avg(avg_tmpr_c#198), 1) AS Average_temp#347, max(avg_tmpr_c#198) AS Max_temp#349, min(avg_tmpr_c#198) AS Min_temp#351]
   +- StreamingRelation DataSource(org.apache.spark.sql.SparkSession@2f6fabc3,delta,List(),None,List(),None,Map(path -> /tmp/delta/bronze/data),None), tahoe, [address#197, avg_tmpr_c#198, avg_tmpr_f#199, city#200, country#201, geoHash#202, id#203, latitude#204, longitude#205, name#206, wthr_date#207, year#208, month#209, day#210]

== Optimized Logical Plan ==
Sort [Hotel_count#345L DESC NULLS LAST], true
+- Aggregate [city#200, wthr_date#207], [city#200, wthr_date#207 AS date#225, approx_count_distinct(id#203, 0.05) AS Hotel_count#345L, round(avg(avg_tmpr_c#198), 1) AS Average_temp#347, max(avg_tmpr_c#198) AS Max_temp#349, min(avg_tmpr_c#198) AS Min_temp#351]
   +- Project [avg_tmpr_c#198, city#200, id#203, wthr_date#207]
      +- StreamingRelation DataSource(org.apache.spark.sql.SparkSession@2f6fabc3,delta,List(),None,List(),None,Map(path -> /tmp/delta/bronze/data),None), tahoe, [address#197, avg_tmpr_c#198, avg_tmpr_f#199, city#200, country#201, geoHash#202, id#203, latitude#204, longitude#205, name#206, wthr_date#207, year#208, month#209, day#210]

== Physical Plan ==
Sort [Hotel_count#345L DESC NULLS LAST], true, 0
+- Exchange rangepartitioning(Hotel_count#345L DESC NULLS LAST, 200), ENSURE_REQUIREMENTS, [id=#1911]
   +- *(4) HashAggregate(keys=[city#200, wthr_date#207], functions=[finalmerge_approx_count_distinct(merge buffer#3206) AS approx_count_distinct(id#203, 0.05)#344L, finalmerge_avg(merge sum#3198, count#3199L) AS avg(avg_tmpr_c#198)#346, finalmerge_max(merge max#3201) AS max(avg_tmpr_c#198)#348, finalmerge_min(merge min#3203) AS min(avg_tmpr_c#198)#350], output=[city#200, date#225, Hotel_count#345L, Average_temp#347, Max_temp#349, Min_temp#351])
      +- StateStoreSave [city#200, wthr_date#207], state info [ checkpoint = <unknown>, runId = 0d724499-5177-4e43-8f1a-db60b1d8dc73, opId = 0, ver = 0, numPartitions = 200], Append, 0, 2
         +- *(3) HashAggregate(keys=[city#200, wthr_date#207], functions=[merge_approx_count_distinct(merge buffer#3206) AS buffer#3206, merge_avg(merge sum#3198, count#3199L) AS (sum#3198, count#3199L), merge_max(merge max#3201) AS max#3201, merge_min(merge min#3203) AS min#3203], output=[city#200, wthr_date#207, buffer#3206, sum#3198, count#3199L, max#3201, min#3203])
            +- StateStoreRestore [city#200, wthr_date#207], state info [ checkpoint = <unknown>, runId = 0d724499-5177-4e43-8f1a-db60b1d8dc73, opId = 0, ver = 0, numPartitions = 200], 2
               +- *(2) HashAggregate(keys=[city#200, wthr_date#207], functions=[merge_approx_count_distinct(merge buffer#3206) AS buffer#3206, merge_avg(merge sum#3198, count#3199L) AS (sum#3198, count#3199L), merge_max(merge max#3201) AS max#3201, merge_min(merge min#3203) AS min#3203], output=[city#200, wthr_date#207, buffer#3206, sum#3198, count#3199L, max#3201, min#3203])
                  +- Exchange hashpartitioning(city#200, wthr_date#207, 200), ENSURE_REQUIREMENTS, [id=#1899]
                     +- *(1) HashAggregate(keys=[city#200, wthr_date#207], functions=[partial_approx_count_distinct(id#203, 0.05) AS buffer#3206, partial_avg(avg_tmpr_c#198) AS (sum#3198, count#3199L), partial_max(avg_tmpr_c#198) AS max#3201, partial_min(avg_tmpr_c#198) AS min#3203], output=[city#200, wthr_date#207, buffer#3206, sum#3198, count#3199L, max#3201, min#3203])
                        +- *(1) Project [avg_tmpr_c#198, city#200, id#203, wthr_date#207]
                           +- StreamingRelation tahoe, [address#197, avg_tmpr_c#198, avg_tmpr_f#199, city#200, country#201, geoHash#202, id#203, latitude#204, longitude#205, name#206, wthr_date#207, year#208, month#209, day#210]
 
```  

* To accomplish the task we need to get temperature/hotel_count timeseries for top 10 biggest cities (cities with the biggest ammount of unique hotels).  

* Firstly we need to get the top 10 biggest cities list:  
 ![screenshots](screenshots/m13_7.png)  

* Execution plan for the top 10 of biggest cities dataframe:   
 ![screenshots](screenshots/m13_8.png)


```
== Parsed Logical Plan ==
GlobalLimit 10
+- LocalLimit 10
   +- Sort [Max_Hotel_Count#894L DESC NULLS LAST], true
      +- Aggregate [city#683], [city#683, max(Hotel_count#685L) AS Max_Hotel_Count#894L]
         +- Relation[city#683,date#684,Hotel_count#685L,Average_temp#686,Max_temp#687,Min_temp#688] parquet

== Analyzed Logical Plan ==
city: string, Max_Hotel_Count: bigint
GlobalLimit 10
+- LocalLimit 10
   +- Sort [Max_Hotel_Count#894L DESC NULLS LAST], true
      +- Aggregate [city#683], [city#683, max(Hotel_count#685L) AS Max_Hotel_Count#894L]
         +- Relation[city#683,date#684,Hotel_count#685L,Average_temp#686,Max_temp#687,Min_temp#688] parquet

== Optimized Logical Plan ==
GlobalLimit 10
+- LocalLimit 10
   +- Sort [Max_Hotel_Count#894L DESC NULLS LAST], true
      +- Aggregate [city#683], [city#683, max(Hotel_count#685L) AS Max_Hotel_Count#894L]
         +- Project [city#683, Hotel_count#685L]
            +- Relation[city#683,date#684,Hotel_count#685L,Average_temp#686,Max_temp#687,Min_temp#688] parquet

== Physical Plan ==
AdaptiveSparkPlan isFinalPlan=false
+- TakeOrderedAndProject(limit=10, orderBy=[Max_Hotel_Count#894L DESC NULLS LAST], output=[city#683,Max_Hotel_Count#894L])
   +- HashAggregate(keys=[city#683], functions=[finalmerge_max(merge max#899L) AS max(Hotel_count#685L)#893L], output=[city#683, Max_Hotel_Count#894L])
      +- Exchange hashpartitioning(city#683, 200), ENSURE_REQUIREMENTS, [id=#1776]
         +- HashAggregate(keys=[city#683], functions=[partial_max(Hotel_count#685L) AS max#899L], output=[city#683, max#899L])
            +- FileScan parquet [city#683,Hotel_count#685L] Batched: true, DataFilters: [], Format: Parquet, Location: PreparedDeltaFileIndex[dbfs:/tmp/delta/silver/data], PartitionFilters: [], PushedFilters: [], ReadSchema: struct<city:string,Hotel_count:bigint>
```  

* Final step is to get data for all cities from top 10 biggest cities list:  

  ![screenshots](screenshots/m13_9.png)

* **Paris**: 
![screenshots](screenshots/city-1-1.png)   
![screenshots](screenshots/city-1-2.png)

* **London**:  
![screenshots](screenshots/city-2-1.png)   
![screenshots](screenshots/city-2-2.png)  
* **Barcelona**:   
![screenshots](screenshots/city-3-1.png)   
![screenshots](screenshots/city-3-2.png)  
* **Milan**:  
![screenshots](screenshots/city-4-1.png)   
![screenshots](screenshots/city-4-2.png)  
* **Amsterdam**:  
![screenshots](screenshots/city-5-1.png)   
![screenshots](screenshots/city-5-2.png)  
* **Paddingon**:  
![screenshots](screenshots/city-6-1.png)   
![screenshots](screenshots/city-6-2.png)  
* **New York**:  
![screenshots](screenshots/city-7-1.png)   
![screenshots](screenshots/city-7-2.png)  
* **San Diego**:  
![screenshots](screenshots/city-8-1.png)   
![screenshots](screenshots/city-8-2.png) 
* **Memphis**:  
![screenshots](screenshots/city-9-1.png)   
![screenshots](screenshots/city-9-2.png) 
* **Houston**:  
![screenshots](screenshots/city-10-1.png)   
![screenshots](screenshots/city-10-2.png) 







